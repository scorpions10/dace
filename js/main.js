jQuery(function($) {
	//smooth scroll

	$('.navbar-nav > li:not(".ingore")').click(function(event) {
		event.preventDefault();
		var _this=$(this);
		var target = $(this).find('>a').prop('hash');
		if($(target).length==1)
		 $('html, body').animate({scrollTop: $(target).offset().top}, 500);
	 	$('#estructura').load($(_this).find('a').attr('data-url'));
	});
	$('#noticia1').click(function(event) {
		event.preventDefault();
		$('#estructura').load('../view/noticia1.html');
	});
	$('#noticia2').click(function(event) {
		event.preventDefault();
		$('#estructura').load('../view/noticia2.html');
	});
	$('#noticia5').click(function(event) {
		event.preventDefault();
		$('#estructura').load('../view/noticia5.html');
	});
	$('#noticia4').click(function(event) {
		event.preventDefault();
		$('#estructura').load('../view/noticia4.html');
	});
	$('#noticia6').click(function(event) {
		event.preventDefault();
		$('#estructura').load('../view/noticia6.html');
	});
	$('#noticia7').click(function(event) {
		event.preventDefault();
		$('#estructura').load('../view/noticia7.html');
	});
  $('#noticia3').click(function(event) {
		event.preventDefault();
		$('#estructura').load('../view/noticia3.html');
	});
	$('#pagina2').click(function(event) {
		event.preventDefault();
		$('#estructura').load('../view/pagina2.html');
	});
	$('#pagina3').click(function(event) {
		event.preventDefault();
		$('#estructura').load('../view/pagina3.html');
	});
	$('#cronograma').click(function(event) {
		event.preventDefault();
		$('#estructura').load('./view/timeline.html');
	});
	$('#pagina1').click(function(event) {
		event.preventDefault();
	window.location.href = "../index.html";
	});
	$('#atras').click(function(event) {
		event.preventDefault();
	 window.location.href = "../index.html";
	});
	$('#home').click(function(event) {
		event.preventDefault();
	 window.location.href = "../index.html";
	});
});
